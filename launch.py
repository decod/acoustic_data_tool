#!/usr/bin/env python3

import argparse
import csv
import http.server
import json
import pathlib
import textwrap

from ipaddress import IPv4Address

from acoustic_data_tool import VERSION
from acoustic_data_tool.lib import logger, reader, extractor, batch_processor
from acoustic_data_tool.webapp.CustomHTTPRequestHandler import CustomHTTPRequestHandler


def parse_args():
    parser = argparse.ArgumentParser(description=f"Acoustic Data Extractor {VERSION}")
    parser.add_argument("--json", help="Output result in JSON format where supported (default: plain)", action="store_true")
    subparsers = parser.add_subparsers(title="Subcommands", required=True)

    check_files_parser = subparsers.add_parser("check_files", help="Try to read and parse the files")
    check_files_parser.add_argument("--source_dir", help="Source directory", required=True)
    check_files_parser.set_defaults(func=check_files)

    server_parser = subparsers.add_parser("server", help="Launch server")
    server_parser.add_argument("--host", help="(default: %(default)s)", default="0.0.0.0", type=IPv4Address)
    server_parser.add_argument("--port", help="(default: %(default)s)", default=8000, type=int)
    server_parser.set_defaults(func=launch_server)

    # Common args for 'extract' and 'batch_extract'
    extract_parent_parser = argparse.ArgumentParser(add_help=False)
    extract_parent_parser.add_argument(
        "--buffer",
        help="Number of frames to keep before/after detection (default: %(default)s)",
        default=50,
        type=int,
    )

    extract_parser = subparsers.add_parser("extract", help="Perform a single extraction", parents=[extract_parent_parser])
    extract_parser.add_argument("--source_dir", help="Source directory", required=True)
    extract_parser.add_argument("--target_dir", help="Target directory", required=True)
    extract_parser.add_argument("--source_filename", help="Source filename (without path)")
    extract_parser.add_argument("--date", help="Date (YYYY-MM-DD)")
    extract_parser.add_argument("--time", help="Time (HH:MM:SS)")
    extract_parser.add_argument("--time_code", help="Time since the beginning of the video (HH:MM:SS)")
    extract_parser.add_argument("--frame", help="Detection start frame", type=int)
    extract_parser.add_argument("--num-frames", help="Detection duration, in frames (default: %(default)s)", type=int, default=0)
    extract_parser.set_defaults(func=extract)

    batch_extract_parser = subparsers.add_parser(
        "batch_extract",
        help="Perform multiple extractions",
        description="Extract multiple files. The data is read from a CSV file wich must have the following fields:  name, date, temps_reel, time_code, frame and nb_frame",
        parents=[extract_parent_parser],
    )
    batch_extract_parser.add_argument("--source_dir", help="Source directory", required=True)
    batch_extract_parser.add_argument("--target_dir", help="Target directory", required=True)
    batch_extract_parser.add_argument("--csv_file", help="CSV file to use", required=True)
    batch_extract_parser.add_argument("--csv_delimiter", help="CSV field separator to use (default: `%(default)s`)", default=",")
    batch_extract_parser.add_argument("--csv_quotechar", help="CSV string delimiter to use (default: `%(default)s`)", default="'")
    batch_extract_parser.add_argument("--filter", help="Optional Python filter (see README.md)")
    batch_extract_parser.set_defaults(func=batch_extract)

    return parser.parse_args()


def check_files(args):
    dr = reader.DirReader(args.source_dir)
    dr.scan()
    for fr in dr.file_readers.values():
        status = "OK"
        details = ""
        if fr.logger.got_error:
            status = "error"
            details = fr.logger.log
        elif fr.logger.got_warning:
            status = "warning"
            details = fr.logger.log
        if fr.inconsistent_dates_msg:
            status = fr.inconsistent_dates_msg
            details = ""
        print(f"{fr.source_filename}: {status}")
        if details:
            print(textwrap.indent(details, "\t"))


def launch_server(args):
    with http.server.HTTPServer((str(args.host), args.port), CustomHTTPRequestHandler) as httpd:
        ip, port = httpd.server_address
        print(f"Acoustic Data Extractor Server {VERSION} running on http://" + ip + ":" + str(port))
        try:
            httpd.serve_forever()
        except KeyboardInterrupt:
            print("\nGot KeyboardInterrupt")
            pass


def extract(args):
    dr = reader.DirReader(args.source_dir)
    dr.scan()
    s = extractor.Extractor(dr, args.target_dir)
    result = s.process_csv_line(
        args.buffer,
        args.num_frames,
        args.source_filename,
        args.date,
        args.time,
        args.time_code,
        args.frame,
    )
    if args.json:
        print(json.dumps(result))
    else:
        for k, v in result.items():
            print(f"{k}: {v}")


def batch_extract(args):
    bp = batch_processor.BatchProcessor(args.source_dir, args.target_dir)
    with open(args.csv_file, newline="") as csv_fp:
        reader = csv.DictReader(csv_fp, delimiter=args.csv_delimiter, quotechar=args.csv_quotechar)
        if args.filter:
            print(f"Using filter `{args.filter}`")
            filtered_lines = filter(eval("lambda line: " + args.filter), reader)
            result = bp.process_csv_lines(args.buffer, filtered_lines)
        else:
            result = bp.process_csv_lines(args.buffer, reader)
    if args.json:
        print(json.dumps(result))
    else:
        line_results = result.pop("result")
        for k, v in result.items():
            print(f"{k}: {v}")
        print("Results for each line:")
        for lr in line_results:
            print("\n---\n")
            for k, v in lr.items():
                print(f"{k}: {v}")


if __name__ == "__main__":
    args = parse_args()
    args.func(args)
