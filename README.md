# acoustic_data_tool

A tool for processing data from acoustic cameras in DIDSON or ARIS format. The main use case is to extract frames before and after fish detections.

## Usage

You can start the web interface by running `./launch.py server`. By default, it will be available on http://localhost:8000.

You can also perform an extraction directly from the command line, for example:

```sh
./launch.py extract --source_dir /path/to/source --target_dir /path/to/target --source_filename 2019-10-23_120000_HF.ddf --frame 100 --buffer 10
```

If you ask for frames outside of the current file the tool will try to find them on the other files. For example, if you pass `--frame 10 --buffer 30`, you're asking for 30 frames before the frame number 10. 20 of those frames are not in the current file, so the tool will try to find a previous file, using the dates parsed from the frame headers.

Each command supports multiple options, run `./launch.py -h` or `./launch.py <subcommand> -h` for a complete list.

### Batch processing

This tool can read CSV files produced by an internal tool at the DECOD UMR. Only a subset of the fields is required, so external users can also use CSVs easily:

```sh
./launch.py batch_extract --source_dir /path/to/source --target_dir /path/to/target --csv_file /path/to/csv
```

Supported fields:

| CSV          | `./launch.py extract` | Description                                                   |
| ------------ | --------------------- | ------------------------------------------------------------- |
| `name`       | `--source_filename`   | Source filename (without path)                                |
| `date`       | `--date`              | Date (YYYY-MM-DD)                                             |
| `temps_reel` | `--time`              | Time (HH:MM:SS)                                               |
| `time_code`  | `--time_code`         | Time since the beginning of the video (HH:MM:SS)              |
| `frame`      | `--frame`             | Detection start frame                                         |
| `nb_frame`   | `--num_frames`        | Detection duration, in frames (default: 0)                    |

If some fields are missing, the tool will try to determine them using the provided fields.

At least one of the following must be provided:

- `name`
- `date` and `temps_reel`

At least one of the following must be provided:

- `frame`
- `date` and `temps_reel`
- `time_code`

If multiple choices are provided, the tool will use the first valid choice, according to the order in the previous lists.

#### `name` processing

The `name` column on CSV files produced by the internal tool isn't a filename, so some processing is needed to transform it into a filename. This processing is done only when `name` matches the following regular expressions:

- `ARIS_(.*)_obd_.*`
- `DIDSON_(.*)_obd_.*`

#### Filtering CSV (CLI)

When using the `batch_extract` command, you can add a filter to keep only certain lines. The filter is a Python expression that will be evaluated for each line in the CSV. If the result is `True`, the line will be kept, else it will be dropped. The current line is available as a dict named `line`.

For example, `--filter 'line["espece1"] == "SAT"'` will keep only lines where the column `espece1` has the exact value `SAT`.

You can combine conditions, for example `--filter 'line["espece1"] == "SAT" and float(line["direction"]) <= 0'`. The CSV fields are parsed as strings, so in order to compare then to a number, we need to first make a conversion to an appropriate type (with `float()`, `int()`, etc.)

Be careful with single (`'`) and double (`"`) quotes. For example, `--filter "line["espece1"] == "SAT""` will not work as intended, your shell will likely evaluate `"line["espece1"] == "SAT""` as `line[espece1] == SAT` before passing the argument to Python.

## Security

Only trusted users should have access to the tool. Currently, the webapp does not limit read/write access to the filesystem, and any string passed to `--filter` can be used to execute arbitrary code as the current user.

## Dev notes

- Lint the code: `black --line-length 140 /path/to/repo`
- Launch tests
  - Extract test ddf files: `cd acoustic_data_tool/tests/integration; tar -xJf fixtures.tar.xz`
  - Return to the root of the repo and run `python -m unittest acoustic_data_tool/tests/integration/test.py`
