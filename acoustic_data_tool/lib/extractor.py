#!/usr/bin/env python3

import os
import sys
import textwrap
import traceback
from bisect import bisect
from datetime import datetime, timedelta

from acoustic_data_tool.lib import logger, reader


class Extractor(object):

    def __init__(self, dr: reader.DirReader, target_dir: str):
        self.dr = dr
        self.target_dir = target_dir
        self.target_filename = ""
        self.multifile = False
        self.logger = logger.Logger()

    def process_csv_line(
        self,
        buffer: int,
        num_frames: int,
        source_filename: str = None,
        date: str = None,
        temps_reel: str = None,
        time_code: str = None,
        frame: int = None,
    ):
        try:
            if not ((date and temps_reel) or time_code or frame):
                self.logger.error("One of ( 'date' and 'temps_reel' ), 'time_code' or 'frame' must be given")

            if not ((date and temps_reel) or source_filename):
                self.logger.error("One of ( 'date' and 'temps_reel' ) or 'source_filename' must be given")

            target_date = None

            if date and temps_reel:
                year, month, day = date.split("-")
                hour, minute, second = temps_reel.split(":")
                target_date = datetime(int(year), int(month), int(day), int(hour), int(minute), int(second))

            if not source_filename:
                source_filename = self.__find_source_filename(target_date)

            if not self.dr.file_readers.get(source_filename):
                self.logger.error(f"File not found: {source_filename}")

            self.fr = self.dr.file_readers[source_filename]
            self.logger = self.fr.logger

            if not frame:
                # Dates from first and last frames, floored/ceiled to seconds
                first_date = self.fr.frame_dates[0].replace(microsecond=0)
                last_date = self.fr.frame_dates[-1].replace(microsecond=0) + timedelta(seconds=1)

                if not target_date:
                    # If target_date has not been already determined from date
                    # and temps_reel, determine it from time_code
                    hours, minutes, seconds = time_code.split(":")
                    target_date = first_date + timedelta(hours=int(hours), minutes=int(minutes), seconds=int(seconds))
                    pass

                if target_date < first_date:
                    self.logger.error(
                        "Target date (%s) older than first frame (%s)",
                        (target_date.isoformat(), first_date.isoformat()),
                    )
                if target_date > last_date:
                    self.logger.error(
                        "Target date (%s) newer than last frame (%s)",
                        (target_date.isoformat(), last_date.isoformat()),
                    )

                frame = bisect(self.fr.frame_dates, target_date)

            self.__extract(frame, num_frames, buffer)
        except logger.LoggerError:
            # Expected error
            pass
        except Exception:
            exc_info = sys.exc_info()
            traceback_str = "".join(traceback.format_exception(*exc_info))
            print(traceback_str)
            self.logger.traceback(traceback_str)
        finally:
            result = {
                "status": self.__status(),
                "log": self.logger.log,
                "source_filename": source_filename,
                "target_filename": self.target_filename,
                "multifile": self.multifile,
            }

            return result

    def __find_source_filename(self, target_date: datetime):
        for fr in self.dr.file_readers.values():
            # Dates from first and last frames, floored/ceiled to seconds
            first_date = fr.frame_dates[0].replace(microsecond=0)
            last_date = fr.frame_dates[-1].replace(microsecond=0) + timedelta(seconds=1)
            if first_date <= target_date <= last_date:
                return fr.source_filename
        self.logger.error(f"Can't find a file with requested date: {target_date.isoformat()}")

    def __find_previous_file(self):
        current_file_first_date = self.fr.frame_dates[0]
        for fr in self.dr.file_readers.values():
            last_date = fr.frame_dates[-1]
            if timedelta(seconds=0) <= current_file_first_date - last_date <= timedelta(seconds=10):
                return fr

    def __find_next_file(self):
        current_file_last_date = self.fr.frame_dates[-1]
        for fr in self.dr.file_readers.values():
            first_date = fr.frame_dates[0]
            if timedelta(seconds=0) <= first_date - current_file_last_date <= timedelta(seconds=10):
                return fr

    def __status(self):
        if self.logger.got_error:
            return "error"
        elif self.logger.got_warning:
            return "warning"
        else:
            return "success"

    def __extract(self, frame: int, num_frames: int, buffer: int):
        """
        Frame count begins at 0
        """

        os.makedirs(self.target_dir, exist_ok=True)

        if not 0 <= frame < self.fr.num_frames:
            raise ValueError(f"frame must be in [0; {self.fr.num_frames}[, got {frame}")

        start_frame = frame - buffer
        end_frame = frame + num_frames + buffer
        prev_reader = None
        next_reader = None
        _, ext = os.path.splitext(self.fr.source_filename)
        target_filename_base = self.fr.frame_dates[frame].strftime("%Y-%m-%d_%H%M%S")
        self.target_filename = f"{target_filename_base}{ext}"
        target_path = os.path.join(self.target_dir, self.target_filename)
        self.fr.copy_file_header(target_path)

        if start_frame < 0:
            self.multifile = True
            if prev_reader := self.__find_previous_file():
                prev_start_frame = prev_reader.num_frames - 1 + start_frame
                prev_end_frame = prev_reader.num_frames - 1
                prev_reader.copy_frames(target_path, prev_start_frame, prev_end_frame)
            else:
                self.logger.warning("Cannot find a file before %s in %s", (self.fr.source_filename, self.dr.source_dir))

        # Copy frames from current file
        curr_start_frame = max(0, start_frame)
        curr_end_frame = min(self.fr.num_frames - 1, end_frame)
        self.fr.copy_frames(target_path, curr_start_frame, curr_end_frame)

        if end_frame >= self.fr.num_frames:
            self.multifile = True
            if next_reader := self.__find_next_file():
                next_end_frame = end_frame - self.fr.num_frames - 1
                next_reader.copy_frames(target_path, 0, next_end_frame)
            else:
                self.logger.warning("Cannot find a file after %s in %s", (self.fr.source_filename, self.dr.source_dir))

        # Write metadata
        metadata = textwrap.dedent(
            f"""
            # Metadata for {self.target_filename}
            status={self.__status()}
            source_filename={self.fr.file}
            frame={frame}
            num_frames={num_frames}
            buffer={buffer}
            start_frame={start_frame}
            end_frame={end_frame}
            multifile={self.multifile}
            prev_file={prev_reader.file if prev_reader else None}
            next_file={next_reader.file if next_reader else None}
            """
        ).lstrip()

        with open(os.path.join(self.target_dir, f"{target_filename_base}.txt"), "x") as fp:
            fp.write(metadata)
