#!/usr/bin/env python3

"""
DIDSON/ARIS reader based on format descriptions:
- DIDSON: https://wiki.oceannetworks.ca/download/attachments/49447779/DIDSON%20V5.26.26%20Data%20File%20and%20Ethernet%20Structure.pdf
- ARIS:
  - https://github.com/SoundMetrics/aris-file-sdk/blob/v2.7.2/docs/understanding-aris-data.md
  - https://github.com/SoundMetrics/aris-file-sdk/blob/v2.7.2/type-definitions/C
"""

import os
import re
import struct
import time

from datetime import datetime

from acoustic_data_tool.lib import logger


class NotDDFFile(Exception):
    pass


class FileReader(object):

    def __init__(self, source_dir: str, source_filename: str, parent_logger: logger.Logger = None):
        self.source_filename = source_filename
        self.file = os.path.join(source_dir, source_filename)
        self.logger = parent_logger or logger.Logger()
        self.logger.set_prefix(source_filename + ":")
        self.headers = self.read_ddf_file_header(self.file)

        self.version = self.headers["nVersion"][3]
        if not 3 <= self.version <= 5:
            self.logger.error("Only DDF versions 3, 4 and 5 (ARIS) are supported")

        if self.version == 3:
            self.file_header_length = 512
            self.frame_header_length = 256
        else:
            self.file_header_length = 1024
            self.frame_header_length = 1024

        self.frame_length = self.frame_header_length + self.headers["nNumRawBeams"] * self.headers["nSamplesPerChannel"]

        # Check for DDF3/4, where frame length depends on bHighResolution
        if self.version in [3, 4]:
            if self.headers["bHighResolution"]:
                expected_frame_length = self.frame_header_length + 49152
            else:
                expected_frame_length = self.frame_header_length + 24576
            if expected_frame_length != self.frame_length:
                self.logger.warning("Expected frame length %s, got %s", (expected_frame_length, self.frame_length))

        # Check declared nFrameTotal
        self.num_frames = self._get_num_frames(self.file)
        if self.num_frames != self.headers["nFrameTotal"]:
            self.logger.warning(
                "File header declares %s frames, but there seem to be %s frames",
                (self.headers["nFrameTotal"], self.num_frames),
            )

        self.frame_dates = self.read_frame_dates()

        if sorted(self.frame_dates) != self.frame_dates:
            self.logger.warning("Frames are not in chronological order")

        self.inconsistent_dates_msg = ""
        try:
            # Compare dates in file and frame headers
            file_header_year, file_header_month, file_header_day = self.headers["strDate"][:10].decode().split("-")
            file_header_ymd = datetime(int(file_header_year), int(file_header_month), int(file_header_day))
            first_frame_ymd = self.frame_dates[0].replace(hour=0, minute=0, second=0, microsecond=0)
            if file_header_ymd != first_frame_ymd:
                guess = "-"
                m = re.search(r"[0-9]{4}-[0-9]{2}-[0-9]{2}_[0-9]{6}", source_filename)
                if m:
                    guess = datetime.strptime(m.group(0), "%Y-%m-%d_%H%M%S").isoformat()
                self.inconsistent_dates_msg = f"Inconsistent dates (file header: {file_header_ymd.strftime('%Y-%m-%d')}, first frame: {self.frame_dates[0].isoformat()}). Guess from file name: {guess}"
                self.logger.warning(self.inconsistent_dates_msg)
        except Exception as e:
            self.logger.warning(f"Can't compare dates in file header and first frame: {e}")

    def _check_same_header(self, key, dest_headers):
        if self.headers[key] != dest_headers[key]:
            self.logger.error("%s is not the same. source: %s, dest: %s", (key, self.headers[key], dest_headers[key]))

    def _get_num_frames(self, file):
        """
        Calculates number of frames in a file. Assumes than self.file and file
        have the same nVersion, nNumRawBeams and nSamplesPerChannel
        """
        file_length = os.stat(file).st_size
        return int((file_length - self.file_header_length) / self.frame_length)

    def read_ddf_file_header(self, file):
        headers = {}
        with open(file, "rb") as fp:
            headers["nVersion"] = struct.unpack("4s", fp.read(4))[0]
            if headers["nVersion"][:3] != b"DDF":
                self.logger.info("Not a DDF or ARIS file")
                raise NotDDFFile
            headers["nFrameTotal"] = struct.unpack("I", fp.read(4))[0]
            headers["nFrameRate"] = struct.unpack("I", fp.read(4))[0]
            headers["bHighResolution"] = struct.unpack("?", fp.read(1))[0]
            fp.read(3)
            headers["nNumRawBeams"] = struct.unpack("I", fp.read(4))[0]
            headers["fSampleRate"] = struct.unpack("f", fp.read(4))[0]
            headers["nSamplesPerChannel"] = struct.unpack("I", fp.read(4))[0]
            headers["nReceiverGain"] = struct.unpack("I", fp.read(4))[0]
            headers["nWindowStart"] = struct.unpack("I", fp.read(4))[0]
            headers["nWindowLength"] = struct.unpack("I", fp.read(4))[0]
            headers["bReverse"] = struct.unpack("?", fp.read(1))[0]
            fp.read(3)
            headers["nSN"] = struct.unpack("I", fp.read(4))[0]
            headers["strDate"] = struct.unpack("32s", fp.read(32))[0]

        return headers

    def read_frame_dates(self):
        frame_dates = []
        got_error = False
        st = time.process_time()
        with open(self.file, "rb") as fp:
            for frame_number in range(self.num_frames):
                fp.seek(self.file_header_length + frame_number * self.frame_length)

                nFrameNumber = struct.unpack("I", fp.read(4))[0]
                tFrameTime = struct.unpack("8s", fp.read(8))[0]
                nVersion = fp.read(4)

                if frame_number != nFrameNumber:
                    got_error = True
                    self.logger.warning("Got frame number %s, expected %s", (nFrameNumber, frame_number))

                if nVersion != self.headers["nVersion"]:
                    got_error = True
                    self.logger.warning("frame %s has version %s, expected %s", (nFrameNumber, nVersion, self.headers["nVersion"]))

                nStatus = struct.unpack("I", fp.read(4))[0]
                if self.version in [3, 4]:
                    nYear = struct.unpack("I", fp.read(4))[0]
                    nMonth = struct.unpack("I", fp.read(4))[0]
                    nDay = struct.unpack("I", fp.read(4))[0]
                    nHour = struct.unpack("I", fp.read(4))[0]
                    nMinute = struct.unpack("I", fp.read(4))[0]
                    nSecond = struct.unpack("I", fp.read(4))[0]
                    nHSecond = struct.unpack("I", fp.read(4))[0]
                    parsed_date = datetime(nYear, nMonth, nDay, nHour, nMinute, nSecond, nHSecond)
                else:
                    sonarTimeStamp = struct.unpack("Q", fp.read(8))[0]
                    sonarTimeStampSeconds = sonarTimeStamp / 1000000
                    parsed_date = datetime.fromtimestamp(sonarTimeStampSeconds)

                frame_dates.append(parsed_date)

        if not got_error:
            et = time.process_time()
            self.logger.info("%s frames read successfully in %s s", (self.num_frames, et - st))

        return frame_dates

    def copy_file_header(self, dest: str):
        with open(self.file, "rb") as src_fp:
            with open(dest, "xb") as dest_fp:
                dest_fp.write(src_fp.read(self.file_header_length))

    def create_test_file(self, dest: str):
        """
        Copies the file and frame headers to a new file, excluding the actual
        data, so that the new file's compressed size is small enough to be added
        to this repo, for use in integration tests
        """
        with open(self.file, "rb") as src_fp:
            with open(dest, "xb") as dest_fp:
                # Copy file header
                dest_fp.write(src_fp.read(self.file_header_length))
                # Copy frame headers
                for frame_number in range(self.num_frames):
                    src_fp.seek(self.file_header_length + frame_number * self.frame_length)
                    dest_fp.seek(self.file_header_length + frame_number * self.frame_length)
                    dest_fp.write(src_fp.read(self.frame_header_length))
                # Add empty body for last frame
                dest_fp.seek(self.file_header_length + self.num_frames * self.frame_length - 1)
                dest_fp.write(b"\x00")

    def copy_frames(self, dest: str, from_frame: int, to_frame: int):
        """
        Copy frames to dest, including from_frame and to_frame

        Frame count begins at 0
        """
        if not 0 <= from_frame < self.num_frames:
            raise ValueError(f"Expected from_frame to be in [0; {self.num_frames}[, got {from_frame}")
        if not 0 <= to_frame < self.num_frames:
            raise ValueError(f"Expected to_frame to be in [0; {self.num_frames}[, got {to_frame}")
        if to_frame < from_frame:
            raise ValueError(f"to_frame must be >= from_frame")

        frames_to_copy = to_frame - from_frame + 1
        dest_headers = self.read_ddf_file_header(dest)

        self._check_same_header("nVersion", dest_headers)
        self._check_same_header("nNumRawBeams", dest_headers)
        self._check_same_header("nSamplesPerChannel", dest_headers)

        with open(self.file, "rb") as src_fp:
            with open(dest, "ab") as dest_fp:
                src_fp.seek(self.file_header_length + from_frame * self.frame_length)
                dest_fp.write(src_fp.read(frames_to_copy * self.frame_length))

        new_num_frames = self._get_num_frames(dest)
        with open(dest, "rb+") as fp:
            # Update nFrameTotal
            fp.seek(4)
            fp.write(struct.pack("I", new_num_frames))

            # Update nFrameNumber
            for frame_number in range(new_num_frames):
                fp.seek(self.file_header_length + frame_number * self.frame_length)
                fp.write(struct.pack("I", frame_number))


class DirReader(object):
    def __init__(self, source_dir: str, parent_logger: logger.Logger = None):
        self.source_dir = source_dir
        self.logger = parent_logger or logger.Logger()
        self.logger.set_prefix(f"DirReader({source_dir}): ")
        self.file_readers = {}

    def scan(self):
        """
        Scans a directory and reads DDF/ARIS files
        The file name and extension doesn't matter, the first 4 bytes of the file
        are used to select DDF/ARIS files
        """
        file_list = os.listdir(self.source_dir)
        for file in file_list:
            if os.path.isfile(os.path.join(self.source_dir, file)):
                try:
                    self.file_readers[file] = FileReader(self.source_dir, file)
                except NotDDFFile:
                    # Expected error
                    pass
