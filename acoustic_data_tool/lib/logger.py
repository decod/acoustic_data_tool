#!/usr/bin/env python3


class LoggerError(Exception):
    pass


class Logger(object):
    def __init__(self):
        self.log = ""
        self.prefix = ""
        self.seen_msgs = {}
        self.got_error = False
        self.got_warning = False

    def __log_with_limit(self, msg, fmt):
        """
        Adds up to 10 similar messages to the log
        """
        if fmt:
            final_msg = msg % fmt
        else:
            final_msg = msg
        if not self.seen_msgs.get(msg):
            self.seen_msgs[msg] = 0
        self.seen_msgs[msg] += 1
        if self.seen_msgs[msg] < 10:
            self.log += final_msg + "\n"
        elif self.seen_msgs[msg] == 10:
            self.log += f"{final_msg}\n  (future similar messages will be hidden)\n"

    def set_prefix(self, prefix):
        self.prefix = prefix

    def traceback(self, msg, fmt=None):
        self.__log_with_limit(f"{msg}", fmt)
        self.got_error = True

    def error(self, msg, fmt=None):
        self.__log_with_limit(f"[ERROR] {self.prefix} {msg}", fmt)
        self.got_error = True
        raise LoggerError()

    def warning(self, msg, fmt=None):
        self.__log_with_limit(f"[WARNING] {self.prefix} {msg}", fmt)
        self.got_warning = True

    def info(self, msg, fmt=None):
        self.__log_with_limit(f"[INFO] {self.prefix} {msg}", fmt)

    def debug(self, msg, fmt=None):
        self.__log_with_limit(f"[DEBUG] {self.prefix} {msg}", fmt)
