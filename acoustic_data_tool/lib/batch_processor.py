#!/usr/bin/env python3

import re
import sys
import traceback

from acoustic_data_tool.lib import logger, reader, extractor


class BatchProcessor(object):

    def __init__(self, source_dir, target_dir):
        self.logger = logger.Logger()
        self.source_dir = source_dir
        self.target_dir = target_dir

    def __fix_names(self, lines):
        """
        Fix names in CSV files produced by an internal tool
        """
        for line in lines:
            res = re.search(r"ARIS_(.*)_obd_.*", line["name"])
            if res:
                line["name"] = res.groups()[0] + ".aris"
            res = re.search(r"DIDSON_(.*)_obd_.*", line["name"])
            if res:
                line["name"] = res.groups()[0] + ".ddf"
            yield line

    def __to_int(self, input):
        """
        Converts input to int, assuming that objects interpreted as False (such
        as the empty string) are 0
        """
        if input:
            return int(input)
        else:
            return 0

    def process_csv_lines(self, buffer, csv_lines):
        try:
            line_results = []
            dr = reader.DirReader(self.source_dir, self.logger)
            dr.scan()
            lines = self.__fix_names(csv_lines)
            for line in lines:
                s = extractor.Extractor(dr, self.target_dir)
                line_result = s.process_csv_line(
                    self.__to_int(buffer),
                    self.__to_int(line["nb_frame"]),
                    line["name"],
                    line["date"],
                    line["temps_reel"],
                    line["time_code"],
                    self.__to_int(line["frame"]),
                )
                line_results.append(line_result)
            self.logger.info(f"Processed {len(line_results)} lines")
        except logger.LoggerError:
            # Known error, will be on the logs
            pass
        except Exception:
            exc_info = sys.exc_info()
            traceback_str = "".join(traceback.format_exception(*exc_info))
            print(traceback_str)
            self.logger.traceback(traceback_str)
        finally:
            if self.logger.got_error:
                status = "error"
            elif self.logger.got_warning:
                status = "warning"
            else:
                status = "success"

            result = {
                "status": status,
                "log": self.logger.log,
                "source_dir": self.source_dir,
                "target_dir": self.target_dir,
                "result": line_results,
            }

            return result
