import json
import os
import subprocess
import tempfile
import unittest

from acoustic_data_tool.lib import reader
import launch


class TestReader(unittest.TestCase):
    def setUp(self):
        # Load test data
        self.base_dir = os.getcwd()
        self.launcher = os.path.join(self.base_dir, "launch.py")
        self.fixtures = os.path.join(self.base_dir, "acoustic_data_tool/tests/integration/fixtures")
        self.dr = reader.DirReader(self.fixtures)
        self.dr.scan()

    def test_header_parsing(self):
        self.assertEqual(
            self.dr.file_readers["2019-10-23_120000_HF.ddf"].headers,
            {
                "nVersion": b"DDF\x03",
                "nFrameTotal": 4485,
                "nFrameRate": 5,
                "bHighResolution": True,
                "nNumRawBeams": 96,
                "fSampleRate": 37286.92578125,
                "nSamplesPerChannel": 512,
                "nReceiverGain": 40,
                "nWindowStart": 17,
                "nWindowLength": 3,
                "bReverse": True,
                "nSN": 656,
                "strDate": b"2019-10-23\x0031019\\2019-10-23_1200",
            },
        )


class TestCLI(unittest.TestCase):
    def setUp(self):
        # Load test data
        self.base_dir = os.getcwd()
        self.launcher = os.path.join(self.base_dir, "launch.py")
        self.fixtures = os.path.join(self.base_dir, "acoustic_data_tool/tests/integration/fixtures")
        self.test_csv = os.path.join(self.base_dir, "acoustic_data_tool/tests/integration/test.csv")

    def test_cli_extract_with_missing_args_fails(self):
        with tempfile.TemporaryDirectory() as tmpdir:
            res = subprocess.run([self.launcher, "extract", "--source_dir", self.fixtures, "--target_dir", tmpdir], capture_output=True)
            self.assertRegex(res.stdout.decode(), r"\[ERROR\]  One of .* must be given")

    def test_cli_extract(self):
        with tempfile.TemporaryDirectory() as tmpdir:
            res = subprocess.run(
                [
                    self.launcher,
                    "extract",
                    "--source_dir",
                    self.fixtures,
                    "--target_dir",
                    tmpdir,
                    "--source_filename",
                    "2019-12-31_120000_HF.ddf",
                    "--frame",
                    "100",
                ],
                capture_output=True,
            )
            # Check that a DDF file is created and that we can read it
            self.assertEqual(os.path.isfile(os.path.join(tmpdir, "2019-12-31_120021.ddf")), True)
            self.assertIsNotNone(reader.FileReader(tmpdir, "2019-12-31_120021.ddf").headers)
            # Check that a metadata file is created and check some of it's data
            metadata_file_path = os.path.join(tmpdir, "2019-12-31_120021.txt")
            self.assertEqual(os.path.isfile(metadata_file_path), True)
            with open(metadata_file_path) as fp:
                metadata_file_contents = fp.read()
            self.assertIn("status=success", metadata_file_contents)
            self.assertIn("frame=100", metadata_file_contents)
            self.assertIn("num_frames=0", metadata_file_contents)
            self.assertIn("start_frame=50", metadata_file_contents)
            self.assertIn("end_frame=150", metadata_file_contents)
            # Check the stdout
            self.assertIn("status: success", res.stdout.decode())

    def test_cli_batch_extract(self):
        with tempfile.TemporaryDirectory() as tmpdir:
            res = subprocess.run(
                [
                    self.launcher,
                    "--json",
                    "batch_extract",
                    "--source_dir",
                    self.fixtures,
                    "--target_dir",
                    tmpdir,
                    "--csv_file",
                    self.test_csv,
                ],
                capture_output=True,
            )
            parsed_stdout = json.loads(res.stdout)
            self.assertEqual(parsed_stdout["status"], "success")
            self.assertEqual(parsed_stdout["result"][0]["status"], "success")
            self.assertEqual(parsed_stdout["result"][1]["status"], "success")
            self.assertEqual(parsed_stdout["result"][2]["status"], "warning")
            self.assertEqual(parsed_stdout["result"][3]["status"], "success")
            self.assertEqual(parsed_stdout["result"][4]["status"], "warning")
            self.assertEqual(parsed_stdout["result"][5]["status"], "warning")
            self.assertEqual(parsed_stdout["result"][6]["status"], "error")
            self.assertEqual(parsed_stdout["result"][7]["status"], "error")


if __name__ == "__main__":
    unittest.main()
