var userIsEditingTextInput = false;

function initializeFormTable(columns) {
    // Step-up headers
    var tableHead = document.getElementById("detections").getElementsByTagName('thead')[0];
    var newRow = tableHead.insertRow();
    columns.forEach(c => {
        var header = document.createElement("div");
        header.innerHTML = c.label;
        if (c.attrs.required) {
            header.innerHTML += ' <span class="req-mark">*</span>'
        }

        var cell = newRow.insertCell();
        cell.classList = ["input_" + c.name];
        cell.appendChild(header);
    });

    // Add a first line to the table
    addFormLine(columns);

    // Listener for keyboard presses
    document.addEventListener('keydown', function (event) {
        if (!userIsEditingTextInput) {
            if (event.key === 'n' || event.key === 'N') {
                addFormLine(columns);
            }
        }
    });

    // Pre-fill global params
        document.getElementById("src_folder").value = localStorage.getItem("src_folder");
        document.getElementById("dest_folder").value = localStorage.getItem("dest_folder");
        document.getElementById("buffer").value = getItemOrDefault("buffer", 50);
}

function getItemOrDefault(item, default_val) {
    localItem = localStorage.getItem(item);
    if (localItem) {
        return localItem
    } else {
        return default_val
    }
}

function addFormLine(columns) {
    var tableBody = document.getElementById("detections").getElementsByTagName('tbody')[0];
    var newRow = tableBody.insertRow();

    columns.forEach(c => {
        addInputCell(newRow, c.name, c.attrs);
    });
    addDeleteButton(newRow);
}

function encodeName(inputName) {
  // KeePassXC browser extension adds a button on fields with 'code' in the name
  // To avoid this, we replace 'code' with a specific string
  return inputName.replace("code", "9cf611e9")
}

function decodeName(inputName) {
  return inputName.replace("9cf611e9", "code")
}

function addInputCell(row, inputName, attrs) {
    var newInput = document.createElement("input");
    newInput.name = encodeName(inputName);
    if (attrs) {
        for (const [key, value] of Object.entries(attrs)) {
            newInput[key] = value;
        }
    }
    newInput.onfocus = () => { userIsEditingTextInput = true; console.error("TEXT INPUT") };
    newInput.onblur = () => { userIsEditingTextInput = false; console.error("TEXT DONE") };
    var cell = row.insertCell();
    cell.classList = ["input_" + inputName];
    cell.appendChild(newInput);
}

function addDeleteButton(row) {
  var del_btn = document.createElement("button");
  del_btn.classList = ["delete-button"];
  del_btn.type = "button";
  del_btn.onclick = function() { row.remove(); };

  row.insertCell().appendChild(del_btn);
}

function convertToJSONAndPostFrom(columns) {
    var formEl = document.forms.form;
    var formData = new FormData(formEl);

    for (var i = 0; i < formEl.elements.length; i++) {
        if (formEl.elements[i].value === '' && formEl.elements[i].hasAttribute('required')) {
            alert('Des champs requis sont manquants !');
            document.styleSheets[0].addRule("input:invalid:not(:focus)", "background: lightcoral")
            return false;
        }
    }

    document.getElementById("form").style.display = "none"
    document.getElementById("loader").style.display = "flex"

    // Step 1 : get global params
    var srcFolderPath = formData.get("src_folder");
    var destFolderPath = formData.get("dest_folder");
    var buffer = formData.get("buffer");
    localStorage.setItem("src_folder", srcFolderPath);
    localStorage.setItem("dest_folder", destFolderPath);
    localStorage.setItem("buffer", buffer);

    // Step 2: convert form to json array
    var csv_lines = [];
    var tableBody = document.getElementById("detections").getElementsByTagName('tbody')[0];
    for (var row of tableBody.rows) {
        jsonObject = {}
        for (var input of row.querySelectorAll('td > input')) {
          jsonObject[decodeName(input.name)] = input.value.trim();
        }
        csv_lines.push(jsonObject);
    }

    // Step 3: post
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/extract", true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                var json = JSON.parse(xhr.responseText);
                gotServerResponse(json)
            } else {
                showError("Erreur serveur lors du découpage *" + xhr.readyState + "(" + xhr.status + ")");
            }
        }
    }
    let jsonForm = {
        "src_folder": srcFolderPath,
        "dest_folder": destFolderPath,
        "buffer": buffer,
        "csv_lines": csv_lines
    }
    xhr.send(JSON.stringify(jsonForm));
}


function gotServerResponse(response) {
    document.getElementById("loader").style.display = "none"
    document.getElementById("result").style.display = "block"
    var resultTableBody = document.getElementById("result").getElementsByTagName('tbody')[1];
    if (response.status == "success" && response.result) {
        document.getElementById("source_dir").innerHTML = response.source_dir
        document.getElementById("target_dir").innerHTML = response.target_dir
        response.result.forEach(r => {
            var lineRow = resultTableBody.insertRow();
            var fileDiv = document.createElement("div");
            var statusDiv = document.createElement("div");
            var targetDiv = document.createElement("div");
            var detailsDiv = document.createElement("div");
            fileDiv.classList = ["filename"];
            statusDiv.classList = ["status"];
            targetDiv.classList = ["filename"];
            detailsDiv.classList = ["details"];

            fileDiv.innerHTML = r.source_filename
            lineRow.classList = ["result_" + r.status];
            targetDiv.innerHTML = r.target_filename;
            if (r.status == "success") {
                statusDiv.innerHTML = "Fichier créé";
            } else if (r.status == "warning") {
                statusDiv.innerHTML = "Warning";
                detailsDiv.innerHTML = r.log.replaceAll("\n", "<br>");
            } else {
                statusDiv.innerHTML = "Erreur";
                detailsDiv.innerHTML = r.log.replaceAll("\n", "<br>");
            }
            lineRow.insertCell().appendChild(fileDiv);
            lineRow.insertCell().appendChild(statusDiv);
            lineRow.insertCell().appendChild(targetDiv);
            lineRow.insertCell().appendChild(detailsDiv);
        })
    } else {
        showError("Erreur serveur lors du découpage (" + response.status + ")");
    }
}

function showError(errorMessage) {
    document.getElementById("loader").style.display = "none"
    document.getElementById("result").style.display = "block"
    var resultTableBody = document.getElementById("result").getElementsByTagName('tbody')[0];
    var error = document.createElement("div");
    error.classList = ["error-message"];
    error.innerHTML = errorMessage
    resultTableBody.insertRow().insertCell().appendChild(error);
}


function loadFromCSV(columns, e) {
    console.log("typeof: " + typeof (columns))
    var file = e.target.files[0]
    var tableBody = document.getElementById("detections").getElementsByTagName('tbody')[0];
    var rowCount = tableBody.rows.length;
    for (var i = 0; i < rowCount; i++) {
        tableBody.deleteRow(0);
    }
    var reader = new FileReader();
    reader.readAsText(file, "UTF-8");
    reader.onload = function (evt) {
        let csvContent = evt.target.result;
        const csvLines = csvContent.split("\n");
        const csvHeaders = csvLines[0].split(",");
        for (var i = 1; i < csvLines.length - 1; i++) {
            const csvColumns = csvLines[i].split(",");
            // Create line
            var newRow = tableBody.insertRow();
            columns.forEach(c => {
                var headerIndex = csvHeaders.indexOf(c.name);
                var attrs = c.attrs
                if (headerIndex > -1) {
                    attrs.defaultValue = csvColumns[headerIndex];
                } else {
                    console.error("Missing CSV value for column " + c.name);
                }

                addInputCell(newRow, c.name, attrs);
            })
            addDeleteButton(newRow);
        }
    };
}
