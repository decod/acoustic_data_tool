var folderTree = {
    name: "/",
    path: "/",
    has_subfolders: false,
    children: []
}

var browserFolderInput = undefined;


function refreshFolderStructure() {
    const folderContainer = document.getElementById('folder-container');

    // Step 1: create a clicable ariadne
    let ariadne = document.createElement('h4')
    const paths = folderTree.path.split("/");
    var currentPath = "";
    for (var i = 1; i < paths.length; i++) {
        currentPath += "/" + paths[i]
        let currentPathLink = document.createElement('span');
        currentPathLink.classList.add('ariadne');
        currentPathLink.innerHTML = " &rarr; " + paths[i];
        const folderPath = currentPath;
        currentPathLink.onclick = () => {
            explorePath(folderPath)
        }
        ariadne.appendChild(currentPathLink);
    }

    ariadne.appendChild(createChooseFolderButton(currentPath));


    // Step 2: show expandable folder tree
    folderContainer.innerHTML = "";
    folderContainer.appendChild(ariadne);
    createFolderList(folderTree, folderContainer);
}

function createFolderList(treeNode, container) {
    treeNode.children.forEach(folder => {
        const folderElement = document.createElement('div');
        folderElement.classList.add('folder');

        const folderNameElement = document.createElement('div');
        folderNameElement.classList.add('folder-name');
        folderNameElement.textContent = folder.name;
        if (folder.permission_error) {
          folderNameElement.textContent = " [locked] " + folderNameElement.textContent
        } else if (folder.has_subfolders) {
            folderNameElement.textContent = " [+] " + folderNameElement.textContent
            folderNameElement.classList.add('has-children');
            folderElement.onclick = () => {
                explorePath(folder.path);
            }
        }
        folderElement.appendChild(folderNameElement);
        folderElement.appendChild(createChooseFolderButton(folder.path));

        container.appendChild(folderElement);
    });
}

async function getSubFoldersForPath(path) {
    return new Promise((resolve, reject) => {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "/listdir?dir=" + path, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    var json = JSON.parse(xhr.responseText);
                    resolve(json)
                } else {
                    reject()
                }
            }
        }
        xhr.send()
    });
}

function exploreFolder(event, folderId, folderType, explanations) {
    event.preventDefault()
    event.stopPropagation()
    document.getElementById('folder_type').innerHTML = folderType
    document.getElementById('explanations').innerHTML = explanations;
    document.getElementById('folder_explorer_modal').style.display = 'flex';
    browserFolderInput = document.getElementById(folderId);
    var folderTypedText = browserFolderInput.value;
    if (!folderTypedText) {
        folderTypedText = "./"
    }
    explorePath(folderTypedText)
}

async function explorePath(path) {
    folderTree.path = path;
    try {
        folderTree.children = await getSubFoldersForPath(path);
    } catch (error) {
        console.error(error);
        folderTree.path = "./";
        folderTree.children = await getSubFoldersForPath("./")

    }
    refreshFolderStructure()
}

function createChooseFolderButton(path) {
    const chooseFolderButton = document.createElement('button');
    chooseFolderButton.classList.add("choose-folder-button")
    chooseFolderButton.classList.add("basic-button")
    chooseFolderButton.textContent = "Choisir ce dossier"
    chooseFolderButton.onclick = () => {
        browserFolderInput.value = path
        document.getElementById('folder_explorer_modal').style.display = 'none';
    }
    return chooseFolderButton;
}

// Click outisde will close modal
window.addEventListener('click', (event) => {
    var modal = document.getElementById('folder_explorer_modal');
    if (event.target === modal) {
        modal.style.display = 'none';
    }
});
