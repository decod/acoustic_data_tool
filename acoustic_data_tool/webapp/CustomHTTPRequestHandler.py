import http.server
import json
import os
from urllib.parse import urlparse, parse_qs

from acoustic_data_tool.lib import batch_processor

DIRECTORY = "acoustic_data_tool/webapp/static/"


class CustomHTTPRequestHandler(http.server.SimpleHTTPRequestHandler):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, directory=DIRECTORY, **kwargs)

    def __list_folders(self, source_dir):
        folder_list = []
        for item in sorted(os.scandir(source_dir), key=lambda x: x.name.lower()):
            if item.is_dir():
                permission_error = False
                has_subfolders = False
                try:
                    subitems = os.scandir(item.path)
                    has_subfolders = any(subitem.is_dir() for subitem in subitems)
                except PermissionError:
                    permission_error = True
                folder_info = {
                    "name": item.name,
                    "path": item.path,
                    "has_subfolders": has_subfolders,
                    "permission_error": permission_error,
                }
                folder_list.append(folder_info)
        return folder_list

    def do_GET(self):
        parsed_url = urlparse(self.path)
        parsed_query = parse_qs(parsed_url.query)
        if parsed_url.path == "/listdir":
            source_dir = parsed_query["dir"][0]
            folder_list = self.__list_folders(source_dir)
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            self.wfile.write(json.dumps(folder_list).encode())
            return
        if parsed_url.path == "/":
            self.path = "/form.html"
        return http.server.SimpleHTTPRequestHandler.do_GET(self)

    def do_POST(self):
        parsed_url = urlparse(self.path)
        if parsed_url.path == "/extract":
            print("Got extraction request")
            # Step 1: read posted JSON
            ctype = self.headers.get_content_type()
            if ctype != "application/json":
                self.send_response(400)
                self.end_headers()
                return
            length = int(self.headers.get("content-length"))
            data = json.loads(self.rfile.read(length))
            # Step 2: extract accordingly
            bp = batch_processor.BatchProcessor(data["src_folder"], data["dest_folder"])
            result = bp.process_csv_lines(data["buffer"], data["csv_lines"])

            # Step 3: send response
            print("Extraction success : " + str(len(result["result"])) + " lines handled")
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            self.wfile.write(json.dumps(result).encode())
