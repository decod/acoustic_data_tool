#!/bin/sh

# Usage: ./release.sh <version>

set -e

cd $(dirname $0)

[ -z $1 ] && echo "Error, empty version." && exit 1

sed -i "s/^__version__.*/__version__ = \"$1\"/" acoustic_data_tool/__init__.py
git add acoustic_data_tool/__init__.py
git commit -m "Bump version to $1"
git tag $1
git push origin
git push origin $1
